package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class registrarRecluso_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css\" integrity=\"sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2\" crossorigin=\"anonymous\">\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Registro Recluso</title>\n");
      out.write("        <link rel=\"stylesheet\"  href=\"css/style.css\">\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div class=\"container clase5\">\n");
      out.write("            <form>\n");
      out.write("\n");
      out.write("                <p class=\"text-center  \">Registro Recluso.</p>\n");
      out.write("                <p >Datos de persona</p>\n");
      out.write("                <div class=\"form-row\">\n");
      out.write("                    <div class=\"form-group col-md-6\">\n");
      out.write("                        <label for=\"inputAddress\">Nombre</label>\n");
      out.write("                        <input type=\"text\" class=\"form-control\" id=\"nombrePersona\" name=\"nombre\">\n");
      out.write("                       \n");
      out.write("                    </div>\n");
      out.write("                    <div class =\"form-group col-md-6\">\n");
      out.write("                        <label for=\"inputAddress\">Apellido</label>\n");
      out.write("                        <input type=\"text\" class=\"form-control\" id=\"nombrePersona\" name=\"apellido\">\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("                <div class=\"form-row\">\n");
      out.write("                    <div class=\"form-group col-md-6\">\n");
      out.write("                        <label for=\"cedulapersona\">Cedula</label>\n");
      out.write("                        <input type=\"number\" class=\"form-control\" id=\"cedulapersona\" name=\"cedula\">\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"form-group col-md-6\">\n");
      out.write("                        <label for=\"telefonopersona\">Telefono</label>\n");
      out.write("                        <input type=\"number\" class=\"form-control\" id=\"telefonopersona\" name=\"telefono\">\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("                <div class=\"form-row\">\n");
      out.write("                    <div class=\"form-group col-md-6\">\n");
      out.write("                        <label for=\"emainpersona\">Email</label>\n");
      out.write("                        <input type=\"email\" class=\"form-control\" id=\"emailpersona\" name=\"email\">\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"form-group col-md-6\">\n");
      out.write("                        <label for=\"direccionpersona\">Direccion</label>\n");
      out.write("                        <input type=\"text\" class=\"form-control\" id=\"direccionpersona\" name=\"direccion\">\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"form-row\">\n");
      out.write("                    <div class=\"form-group col-md-6\">\n");
      out.write("                        <label for=\"telefonofamiliar\">TelefonoFamiliar</label>\n");
      out.write("                        <input type=\"tel\" class=\"form-control\" id=\"telefonofamiliar\" name=\"telefonoFamiliar\">\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"form-group col-md-6\">\n");
      out.write("                        <label for=\"inputAddress\">Fecha Ingreso</label>\n");
      out.write("                        <input type=\"date\" class=\"form-control\" id=\"fechaingreso\" name=\"fechaIngreso\">\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("                <p >Datos de Sentencia</p>\n");
      out.write("\n");
      out.write("                <div class=\"form-row\">\n");
      out.write("                    <div class=\"form-group\">\n");
      out.write("                        <label >  Descripcion </label>\n");
      out.write("                        <textarea class=\"form-control \" id=\"descripcionsentencia\" rows=\"5\" name=\"sentencia\" ></textarea>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"form-row\">\n");
      out.write("                    <div class=\"form-group col-md-6\">\n");
      out.write("                        <label >Tiempo Condena</label>\n");
      out.write("                        <input type=\"text\" class=\"form-control\" id=\"tiempocondena\" name=\"tiempoCadena\">\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"form-group col-md-6 \">\n");
      out.write("                        <label >Estado</label>\n");
      out.write("                            <select class=\"custom-select mr-sm-2\" id=\"estadosentencia\" name=\"estado\">\n");
      out.write("                                <option selected>Choose...</option>\n");
      out.write("                                <option value=\"1\">One</option>\n");
      out.write("                                \n");
      out.write("                            </select>\n");
      out.write("                    </div>\n");
      out.write("                     <div class=\"form-group col-md-6\">\n");
      out.write("                        <label for=\"inputAddress\">Fecha Ingreso</label>\n");
      out.write("                        <input type=\"date\" class=\"form-control\" id=\"fechaingreso\">\n");
      out.write("                    </div>\n");
      out.write("                          <div class=\"form-group col-md-6 \">\n");
      out.write("                        <label >ID Pabellon</label>\n");
      out.write("                            <select class=\"custom-select mr-sm-2\" id=\"idpabellonrecluso\">\n");
      out.write("                                <option selected>Choose...</option>\n");
      out.write("                                <option value=\"1\">One</option>\n");
      out.write("                                <option value=\"2\">Two</option>\n");
      out.write("                                <option value=\"3\">Three</option>\n");
      out.write("                            </select>\n");
      out.write("                    </div> \n");
      out.write("        </div>\n");
      out.write("                <div class=\"form-group col-md-6 \">\n");
      out.write("                        <label >ID Celda</label>\n");
      out.write("                            <select class=\"custom-select mr-sm-2\" id=\"idceldarecluso\">\n");
      out.write("                                <option selected>Choose...</option>\n");
      out.write("                                <option value=\"1\">One</option>\n");
      out.write("                                <option value=\"2\">Two</option>\n");
      out.write("                                <option value=\"3\">Three</option>\n");
      out.write("                            </select>\n");
      out.write("                    </div> \n");
      out.write("\n");
      out.write("                <a type=\"submit\" href=\"reclusos.jsp\" class=\"btn btn-primary form-control\">Registrar</a>\n");
      out.write("            </form>\n");
      out.write("        </div>\n");
      out.write("        <script src=\"https://code.jquery.com/jquery-3.5.1.slim.min.js\" integrity=\"sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx\" crossorigin=\"anonymous\"></script>\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
