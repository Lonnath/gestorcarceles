

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ver Recluso</title>
        <link rel="stylesheet"  href="css/style.css">
    </head>
    <body>
        <div class=" clase5">
            <form>

                <p class="text-center  ">Registro Recluso.</p>
                <p >Datos de persona</p>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputAddress">Nombre</label>
                        <label type="text" class="form-control" id="nombrePersona">JUAN DAVID</label>
                    </div>
                    <div class ="form-group col-md-6">
                        <label for="inputAddress">Apellido</label>
                        <label type="text" class="form-control" id="nombrePersona">PEREZ PEREZ</label>
                    </div>
                </div>


                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="cedulapersona">Cedula</label>
                        <label type="number" class="form-control" id="cedulapersona">1010127648</label>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="telefonopersona">Telefono</label>
                        <label type="number" class="form-control" id="telefonopersona">315845714</label>
                    </div>

                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="emainpersona">Email</label>
                        <label type="email" class="form-control" id="emailpersona">prueba@gamail.com</label>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="direccionpersona">Direccion</label>
                        <label type="text" class="form-control" id="direccionpersona"></label>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="telefonofamiliar">TelefonoFamiliar</label>
                        <label type="tel" class="form-control" id="telefonofamiliar">30003566687 </label>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputAddress">Fecha Ingreso</label>
                        <label type="date" class="form-control" id="fechaingreso">2020/01/22</label>
                    </div>
                </div>


                <p >Datos de Sentencia</p>

                <div class="form-row">
                    <div class="form-group">
                        <label >  Descripcion </label>
                        <textarea   class="form-control " id="descripcionsentencia" rows="5" > Robo con armaa blanca</textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label >Tiempo Condena</label>
                        <label type="text" class="form-control" id="tiempocondena"> 2 años</label>
                    </div>
                    <div class="form-group col-md-6 ">
                        <label >Estado</label>
                            <label >Estado</label>
                        <label type="text" class="form-control" id="estadosentencia">En proceso</label>
                    </div>
                     <div class="form-group col-md-6">
                        <label for="inputAddress">Fecha Ingreso</label>
                        <label type="date" class="form-control" id="fechaingreso"></label>
                    </div>
                      <div class="form-group col-md-6 ">
                        <label >ID Pabellon</label>
                            <select class="custom-select mr-sm-2" id="idpabellonrecluso">
                                <option selected>Choose...</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                    </div> 
        </div>
                <div class="form-group col-md-6 ">
                        <label >ID Celda</label>
                            <select class="custom-select mr-sm-2" id="idceldarecluso">
                                <option selected>Choose...</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                    </div>
                
                
                 

                <a type="submit" href="reclusos.jsp" class="btn btn-primary">Finalizar</a>
            </form>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    </body>
</html>
