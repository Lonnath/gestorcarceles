
package ModeloDAO;

import Config.Conexion;
import Modelo.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class PersonaDAO {
    
    Conexion conect = new Conexion();
    Connection conection;
    PreparedStatement ps;
    ResultSet rs;
    public Persona getPersonaE(long cod){
        
        Persona persona = new Persona();
        String sql = "SELECT * FROM Persona WHERE cedula="+cod;
        try{
            conection = conect.getConnection();
            ps = conection.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                persona.setCedula(rs.getLong("cedula"));
                persona.setNombre(rs.getString("nombre"));
                persona.setApellido(rs.getString("apellido"));
                persona.setCorreo(rs.getString("correo"));
                persona.setTelefono(rs.getInt("telefono"));
            }
        }catch (Exception e){
        }
        return persona;
    
    }
}
