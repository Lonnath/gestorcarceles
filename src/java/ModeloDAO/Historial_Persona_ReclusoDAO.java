
package ModeloDAO;

import Config.Conexion;
import Modelo.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Historial_Persona_ReclusoDAO {

    Conexion conect = new Conexion();
    Connection conection;
    PreparedStatement ps;
    ResultSet rs;

    public Historial_Persona_Recluso getReclusoInfo(int cod) {

        Historial_Persona_Recluso historial = new Historial_Persona_Recluso();
        String sql = "SELECT * FROM historial WHERE codrecluso=" + cod;
        try {
            conection = conect.getConnection();
            ps = conection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                historial.setCedula(rs.getLong("cedula"));
                historial.setCod_recluso(rs.getInt("codrecluso"));
                historial.setFechaingreso(rs.getString("fecha"));
            }
        } catch (Exception e) {
        }
        return historial;
    }
}
