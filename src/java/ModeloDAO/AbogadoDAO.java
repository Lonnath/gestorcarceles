package ModeloDAO;

import Config.Conexion;
import Modelo.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class AbogadoDAO {

    Conexion conect = new Conexion();
    Connection conection;
    PreparedStatement ps;
    ResultSet rs;

    public Abogado getAbogado(int cod) {
        Abogado tmpo = new Abogado();
        String sql = "SELECT * FROM abogado WHERE cedula = '" + cod + "'";
        try {
            conection = conect.getConnection();
            ps = conection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                tmpo.setCedula(rs.getLong("cedula"));
                tmpo.setNombre(rs.getString("nombre"));
                tmpo.setApellido(rs.getString("apellido"));
                tmpo.setDireccion(rs.getString("direccion"));
                tmpo.setTelefono(rs.getInt("telefono"));
                tmpo.setCorreo(rs.getString("correo"));

            }
        } catch (Exception e) {
        }
        return tmpo;

    }
}
