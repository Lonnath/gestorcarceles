package ModeloDAO;

import Config.Conexion;
import Modelo.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CeldaDAO {
    Conexion conect = new Conexion();
    Connection conection;
    PreparedStatement ps;
    ResultSet rs;

    public List<Celda> getCelda(int idPabellon) {
        ArrayList<Celda> celdas = new ArrayList();
        String sql = "SELECT * FROM celda WHERE id_pabellon = '"+idPabellon+"'";
        try {
            conection = conect.getConnection();
            ps = conection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Celda celda = new Celda();
                celda.setId(rs.getInt("id"));
                celda.setId_nombre(rs.getInt("id_nombre"));
                celda.setId_pabellon(rs.getInt("id_pabellon"));
                celda.setCapacidad(rs.getInt("capacidad"));
                celda.setPiso(rs.getInt("piso"));
                celdas.add(celda);
            }
        } catch (Exception e) {
            System.err.println("Celdas: "+e.getMessage());
        }
        return celdas;

    }
}
