
package Modelo;

public class Nivel_Pabellon {
    String nombre;

    public Nivel_Pabellon() {
    }
    
    public Nivel_Pabellon(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
}
