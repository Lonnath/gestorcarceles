/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.*;
import ModeloDAO.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author USUARIO
 */
public class Control extends HttpServlet {

    String login = "index.jsp";
    String recluso = "reclusos.jsp";
    String users = "usuarioAdmin.jsp";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String acceso = "";
        String action = request.getParameter("accion");
        HttpSession sesion = request.getSession();
        /*Usuario user = (Usuario) sesion.getAttribute("user");
        System.out.println("SESION: "+request.getSession().getAttribute("user"));
        sesion.removeAttribute("user");
        System.out.println("SESION: "+request.getSession().getAttribute("user"));
        */
        if(action.equalsIgnoreCase("cerrarSesion")){
            sesion.removeAttribute("user");
            acceso = login;
        }
        
        if(action.equalsIgnoreCase("eliminarUser")){
            UsuarioDAO tmpoDAO = new UsuarioDAO();
            tmpoDAO.eliminateUser(request.getParameter("user"));
            acceso = users;
        }
        
        if(action.equalsIgnoreCase("ingresarUsuario")){
            Usuario user1 = (Usuario)sesion.getAttribute("user");
            String usuario = request.getParameter("email");
            String password = request.getParameter("password");
            String nombre = request.getParameter("nombre");
            String apellido = request.getParameter("apellido");
            long telefono = Long.parseLong(request.getParameter("telefono"));
            String url = request.getParameter("url");
            int cedula = Integer.parseInt(request.getParameter("cedula"));
            Usuario user = new Usuario();
            user.setUsuario(usuario);
            user.setPassword(password);
            user.setNombre(nombre);
            user.setApellido(apellido);
            user.setTelefono(telefono);
            user.setTipoUsuario(1);
            user.setUrl(url);
            user.setCedula(cedula);
            user.setIdcarcel(user1.getIdcarcel());
            acceso = new UsuarioDAO().ingresarUsuario(user);
            System.out.println(acceso);
        }
        
        
        
        RequestDispatcher vista = request.getRequestDispatcher(acceso);
        vista.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
